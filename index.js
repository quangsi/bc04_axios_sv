const BASE_URL = "https://62db6cb3e56f6d82a77285da.mockapi.io";

function getDSSV() {
  batLoading();
  axios({
    url: `${BASE_URL}/sv`,
    method: "GET",
    // data:{}
  })
    .then(function (res) {
      tatLoading();
      console.log(res);
      renderDSSV(res.data);
    })
    .catch(function (err) {
      tatLoading();
      console.log(err);
    });
}
getDSSV();
function xoaSinhVien(id) {
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      console.log(res);
      getDSSV();
    })
    .catch(function (err) {
      console.log(err);
    });
}

function themSV() {
  // newSv = object lấyb từ form
  let newSV = {
    name: "Bánh mì",
    email: "Ceasar3@hotmail.com",
    password: "Hwq1l6Te8JncD6s",
    math: 92396,
    physics: 93970,
    chemistry: 2456,
    id: "17",
  };
  batLoading();
  axios({
    url: `${BASE_URL}/sv`,
    method: "POST",
    data: newSV,
  })
    .then(function (res) {
      tatLoading();
      console.log(res);
    })
    .catch(function (err) {
      tatLoading();
      console.log(err);
    });
}
